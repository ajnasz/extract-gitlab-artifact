package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"

	"gitlab.com/ajnasz/extract-gitlab-artifact/gitlab"
)

var (
	gitlabCommitSHA string
	version         string
)

func filterBuildJobs(jobs []gitlab.Job) []gitlab.Job {
	var out []gitlab.Job

	for _, job := range jobs {
		if job.Stage == "build" {
			out = append(out, job)
		}
	}

	return out
}

func filterSuccessJobs(jobs []gitlab.Job) []gitlab.Job {
	var out []gitlab.Job

	for _, job := range jobs {
		if job.Status == "success" {
			out = append(out, job)
		}
	}

	return out
}

func filterJobByCommit(jobs []gitlab.Job, commitSHA string) []gitlab.Job {
	var out []gitlab.Job

	for _, job := range jobs {
		if job.Commit.ID == commitSHA || job.Commit.Title == commitSHA || job.Commit.ShortID == commitSHA {
			out = append(out, job)
		}
	}

	return out
}

func sortJobs(jobs []gitlab.Job) []gitlab.Job {
	return jobs
}

func getArchive(gitlabToken string, gitlabHost string, projectID int, version string) (*http.Response, error) {
	g := gitlab.New(gitlabToken, gitlabHost)
	res, err := g.DownloadArchive(projectID, version)
	if err != nil {
		if res.StatusCode == 500 {
			str, _ := ioutil.ReadAll(res.Body)
			log.Println(string(str))
		}
		return nil, fmt.Errorf("download artifact failed %w", err)
	}

	return res, nil
}

func getArtifact(gitlabToken string, gitlabHost string, projectID int) (*http.Response, error) {
	g := gitlab.New(gitlabToken, gitlabHost)

	jobs, err := g.GetJobs(projectID)

	if err != nil {
		return nil, fmt.Errorf("Get jobs %w", err)
	}

	if len(jobs) == 0 {
		return nil, errors.New("No jobs found")
	}

	jobs = filterJobByCommit(filterSuccessJobs(filterBuildJobs(jobs)), gitlabCommitSHA)

	if len(jobs) == 0 {
		return nil, errors.New("No valid jobs found")
	}

	sort.Slice(jobs, func(i, j int) bool {
		return jobs[i].FinishedAt.After(jobs[j].FinishedAt)
	})

	lastJob := jobs[0]

	res, err := g.DownloadArtifact(lastJob)

	if err != nil {
		if res.StatusCode == 500 {
			str, _ := ioutil.ReadAll(res.Body)
			log.Println(string(str))
		}
		return nil, fmt.Errorf("download artifact failed %w", err)
	}

	return res, nil
}

type commandArgs struct {
	Version     bool
	Archive     bool
	ProjectID   int
	Commit      string
	Token       string
	Host        string
	Destination string
}

func flagOrEnv(flagPtr *string, envName string) string {
	if flagPtr == nil || *flagPtr == "" {
		return os.Getenv(envName)
	}
	return *flagPtr
}

func getArgs() (*commandArgs, error) {
	versionFlag := flag.Bool("version", false, "Get the version")
	projectIDFlag := flag.Int("projectID", 0, "Gitlab project id")
	archiveFlag := flag.Bool("archive", false, "Download archive instead of a job artifact")
	commitFlag := flag.String("commit", "", "Commit hash or build title")
	tokenFlag := flag.String("token", "", "Gitlab token")
	hostFlag := flag.String("host", "", "Gitlab host")
	destFlag := flag.String("destination", "", "Path to downloaded artifact")
	flag.Parse()

	if *versionFlag {
		return &commandArgs{
			Version: true,
		}, nil
	}

	var projectID int

	if projectIDFlag == nil || *projectIDFlag == 0 {
		os.Getenv("CI_PROJECT_ID")
		projID, err := strconv.Atoi(os.Getenv("CI_PROJECT_ID"))
		if err != nil {
			return nil, err
		}
		projectID = projID
	} else {
		projectID = *projectIDFlag
	}

	commit := flagOrEnv(commitFlag, "CI_COMMIT_SHA")
	token := flagOrEnv(tokenFlag, "GITLAB_TOKEN")
	host := flagOrEnv(hostFlag, "GITLAB_HOST")
	dest, err := filepath.Abs(flagOrEnv(destFlag, "DESTINATION"))

	if err != nil {
		return nil, err
	}

	return &commandArgs{
		Version:     *versionFlag,
		ProjectID:   projectID,
		Archive:     *archiveFlag,
		Commit:      commit,
		Token:       token,
		Host:        host,
		Destination: dest,
	}, nil

}

func downloadBin(args commandArgs) error {
	var res *http.Response
	if args.Archive {
		r, err := getArchive(args.Token, args.Host, args.ProjectID, args.Commit)
		if err != nil {
			return err
		}

		res = r
	} else {
		r, err := getArtifact(args.Token, args.Host, args.ProjectID)
		if err != nil {
			return err
		}

		res = r
	}
	dest, err := os.Create(args.Destination)
	if err != nil {
		log.Fatal(err)
	}

	io.Copy(dest, res.Body)

	dest.Close()

	return nil
}

func main() {
	args, err := getArgs()
	if err != nil {
		fmt.Fprintln(os.Stderr, "invalid arguments", err)
		os.Exit(1)
	}

	if args.Version {
		fmt.Println(version)
		return
	}

	gitlabCommitSHA = args.Commit

	if err := downloadBin(*args); err != nil {
		fmt.Fprintln(os.Stderr, fmt.Errorf("download failed %w", err))
		os.Exit(2)
	}
}
