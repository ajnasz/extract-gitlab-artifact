package gitlab

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"strconv"
)

// Gitlab API struct
type Gitlab struct {
	Token string
	Host  string
}

func (g *Gitlab) callGitlabAPI(queryURL *url.URL) (*http.Response, error) {
	client := &http.Client{}
	queryURL.Host = g.Host
	queryURL.Scheme = "https"
	req, err := http.NewRequest(http.MethodGet, queryURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("PRIVATE-TOKEN", g.Token)

	return client.Do(req)
}

// GetJobs Returns jobs for a project
func (g *Gitlab) GetJobs(projectID int) ([]Job, error) {
	u, err := url.Parse(g.Host)

	if err != nil {
		return nil, err
	}

	u.Path = path.Join("/api/v4/projects/", strconv.Itoa(projectID), "jobs")
	u.RawQuery = "scope=success"

	resp, err := g.callGitlabAPI(u)

	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	res, err := ioutil.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(string(res))
	}

	if err != nil {
		return nil, err
	}

	var jobs []Job
	json.Unmarshal(res, &jobs)

	for i, job := range jobs {
		job.Project = projectID
		jobs[i] = job
	}

	return jobs, nil
}

// getArchiveURL Returns url for a repository archive
func (g Gitlab) getArchiveURL(project int, version string) (*url.URL, error) {
	u, err := url.Parse(g.Host)
	if err != nil {
		return nil, err
	}
	u.Scheme = "https"
	u.Host = g.Host
	u.Path = path.Join("/api/v4/projects/", strconv.Itoa(project), "/repository/archive.zip")
	q := u.Query()
	q.Set("sha", version)
	u.RawQuery = q.Encode()
	return u, nil
}

// getArtifactURL Returns url for artifact
func (g Gitlab) getArtifactURL(job Job) (*url.URL, error) {
	u, err := url.Parse(g.Host)
	if err != nil {
		return nil, err
	}

	u.Scheme = "https"
	u.Host = g.Host
	u.Path = path.Join("/api/v4/projects/", strconv.Itoa(job.Project), "/jobs/", strconv.Itoa(job.ID), "artifacts")

	return u, nil
}

// DownloadArtifact Downloads job artifact
func (g Gitlab) DownloadArtifact(job Job) (*http.Response, error) {
	artifactURL, err := g.getArtifactURL(job)
	if err != nil {
		return nil, err
	}

	return g.Download(artifactURL)
}

// DownloadArchive Downloads job artifact
func (g Gitlab) DownloadArchive(project int, version string) (*http.Response, error) {
	artifactURL, err := g.getArchiveURL(project, version)
	if err != nil {
		return nil, err
	}

	return g.Download(artifactURL)
}

// Download basically calls API
func (g Gitlab) Download(url *url.URL) (*http.Response, error) {
	res, err := g.callGitlabAPI(url)
	if err != nil {
		return res, err
	}

	if res.StatusCode != http.StatusOK {
		return res, fmt.Errorf("HTTP Error: %d", res.StatusCode)
	}

	return res, nil
}

// New Creates a Gitlab instance
func New(token string, host string) *Gitlab {
	return &Gitlab{
		token,
		host,
	}
}
