package gitlab

import (
	"encoding/json"
	"testing"
	"time"
)

type testExpectation struct {
	Name     string
	Actual   interface{}
	Expected interface{}
}

func TestJob(t *testing.T) {
	var job Job

	jobStr := `{"stage":"build", "finished_at": "2015-12-24T17:54:24.921Z", "status": "success", "commit": {"id": "0ff3ae198f8601a285adcf5c0fff204ee6fba5fd"}}`
	json.Unmarshal([]byte(jobStr), &job)

	cases := []testExpectation{
		testExpectation{
			"Stage",
			job.Stage,
			"build",
		},
		testExpectation{
			"Status",
			job.Status,
			"success",
		},
		testExpectation{
			"Commit.ID",
			job.Commit.ID,
			"0ff3ae198f8601a285adcf5c0fff204ee6fba5fd",
		},
		testExpectation{
			"FinishedAt",
			job.FinishedAt.Format(time.RFC3339),
			"2015-12-24T17:54:24Z",
		},
	}

	for _, expectation := range cases {
		if expectation.Actual != expectation.Expected {
			t.Errorf("Expected job.%s to be %s, but it's %s", expectation.Name, expectation.Expected, expectation.Actual)
		}
	}
}
