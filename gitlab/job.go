package gitlab

import "time"

type Job struct {
	Project    int
	ID         int       `json:"id"`
	Stage      string    `json:"stage"`
	FinishedAt time.Time `json:"finished_at"`
	Status     string    `json:"status"`
	Commit     Commit    `json:"commit"`
}
