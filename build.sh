#!/bin/sh


VERSION=$(git describe --tags)
go build -ldflags "-w -s -X main.version=${VERSION}"
